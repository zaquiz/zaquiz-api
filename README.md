# Zaquiz?!

## Prérequis

- ngrok
- python 3
- mongodb

## installation des dépendances

Ouvrez un terminal puis exécutez les commandes suivantes : 

```
cd api
pip install -r requirements.txt
```

## Avant de jouer

- Placez vous d'abord sous le dossier /api puis ouvrez-y un terminal.
- Lancez l'api localement : `python run.py` (si c'est la 1ère fois que vous lancez l'api il faut initialiser la base de données pour cela lancez : `python run.py --init-db`)
- Dans un autre terminal, exécutez ngrok : `ngrok.exe http 5000`
- Ouvrez l'appli android puis dans allez dans settings (icône d'engrenage). Là changez la valeur du ngrok id à celui qui correspond à l'url affiché dans votre second terminal (par exemple: Si votre url est "http://8b63d69a.ngrok.io" votre id sera "8b63d69a")
- Vous pouvez maintenant utiliser l'application ! 
