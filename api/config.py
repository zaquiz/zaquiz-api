from os import getenv

def getenv_as(name, obj_type, default=None):
    """
    Returns an environment variable casted as the given type.
    """
    return obj_type(getenv(name,default=default))

DEBUG = getenv_as("API_DEBUG", bool, False)
MONGO_URI = getenv("MONGO_URI", "mongodb://localhost:27017/zaquiz")
RESOURCES_DIR = getenv("RESOURCES_DIR", "../resources/")