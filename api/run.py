from sys import argv
import json

from app import MAIN_APP
from app.datamodels import Source, Category, Entity
from app.dataservices import SourceService, CategoryService, EntityService

def init_db():
    try:
        with open("./test_data/categories.json") as jsfile:
            categories = json.load(jsfile)
        with open("./test_data/sources.json") as jsfile:
            sources = json.load(jsfile)
        with open("./test_data/entities.json") as jsfile:
            entities = json.load(jsfile)
        CategoryService.insert_many([Category(c) for c in categories])
        SourceService.insert_many([Source(s) for s in sources])
        EntityService.insert_many([Entity(e) for e in entities])
        print("database initialization succeed!")
    except Exception as e:
        print("Error while reading test data : ")
        print(e)
        raise e

if __name__ == "__main__":
    if len(argv) > 1 and argv[1] == "--init-db":
        init_db()
    MAIN_APP.run()