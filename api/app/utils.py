
FIELD_MISSING_MSG = "Field '{0}' is missing."

FIELD_WRONG_TYPE_MSG = "Field '{0}' should be one of the following types: {1}."

DUPLICATE_ID_MSG = "Already existing '{0}' : {1}."

NOT_EXISTING_REFERENCE = "Field '{0}' is referencing to a not existing document : {1}."

FORBIDDEN_UPDATE = "Field '{0}' can't be updated."

class ValidationFailed(Exception):
    """
    Exception class for validation failure.
    """

    def __init__(self,message=""):
        self.message = message
