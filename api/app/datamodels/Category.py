from .DataModel import DataModel

class Category(DataModel):
    """
    DataModel for Category.
    """

    FIELDS = {
        "categoryId": {
            "required": True,
            "types": (str,)
        },
        "parentId": {
            "types": (str,)
        }
    }

    ID_NAME = "categoryId"

    @property
    def categoryId(self) -> str:
        """
        The id of the category.
        """
        return self._data.get("categoryId")
    
    @property
    def parentId(self) -> str:
        """
        The id of the parent category. Can be None.
        """
        return self._data.get("parentId")
