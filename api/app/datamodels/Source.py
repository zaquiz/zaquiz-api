from .DataModel import DataModel
from ..utils import FIELD_WRONG_TYPE_MSG

class Source(DataModel):
    """
    DataModel for Source.
    """

    ID_NAME = "sourceId"

    FIELDS = {
        "sourceId": {
            "required": True,
            "types": (str,)
        },
        "names": {
            "required": True,
            "types": (list,)
        },
        "categoryId": {
            "required": True,
            "types": (str,)
        },
        "links": {
            "types": (list,)
        }
    }

    @property
    def sourceId(self) -> str:
        """
        The id of the source.
        """
        return self._data.get("sourceId")
    
    @property
    def names(self) -> list:
        """
        The valid names for the source.
        """
        return self._data.get("names")
    
    @property
    def categoryId(self) -> str:
        """
        The id of the source's category.
        """
        return self._data.get("categoryId")
    
    @property
    def links(self) -> list:
        """
        A list of useful links related to the source. Can be None.
        """
        return self._data.get("links")
    
    def validate(self) -> tuple:
        """
        Returns a tuple (valid,msg) where valid is a boolean
        to indicates if the data is valid and msg is the corresponding message.
        """

        validation = super().validate()

        if not validation[0]:
            return validation
        
        i = 0
        for name in self.names:
            if not isinstance(name, str):
                return (False, FIELD_WRONG_TYPE_MSG.format(f"names[{i}]", "str"))
            i += 1
        
        i = 0
        for link in self.links or []:
            if not isinstance(link, str):
                return (False, FIELD_WRONG_TYPE_MSG.format(f"links[{i}]", "str"))
            i += 1
        
        return validation