from ..utils import FIELD_MISSING_MSG, FIELD_WRONG_TYPE_MSG

class DataModel:
    """
    Abstract class to represent database entities.
    """

    # This constant must be overriden in every child class.
    # It describes the fields of the model just as follow:
    # "nameOfField": { "required": a boolean, "types": a tuple of valid types }
    # If "types" is absent, any type will be valid.
    # By default a field will not be required.
    FIELDS = {}

    # Describes the name of the model id.
    ID_NAME = "id"

    def __init__(self, data : dict):
        self._data = data
        self.clean()
    
    def id(self) -> str:
        """
        Returns the entity's id.
        """
        return self._data.get(self.ID_NAME)

    def clean(self):
        """
        Removes the unecessary fields in the data.
        This method is called at the creation of the instance.
        """
        allowed_fields = ["_id"] + [*self.FIELDS.keys()]
        for key in self._data.keys():
            if key not in allowed_fields:
                self._data.pop(key)

    def validate(self) -> tuple:
        """
        Returns a tuple (valid,msg) where valid is a boolean
        to indicates if the data is valid and msg is the corresponding message.
        """

        for key, value in self.FIELDS.items():
            data_value = self._data.get(key)

            # Validating required fields.
            if value.get("required") and data_value is None:
                return (False, FIELD_MISSING_MSG.format(key))
            
            allowed_types : tuple = value.get("types")
            # Validating types
            if data_value is not None and allowed_types and not isinstance(data_value, allowed_types):
                required_types = ", ".join([it.__name__ for it in allowed_types])
                return (False, FIELD_WRONG_TYPE_MSG.format(key, required_types))
        
        return (True, "")
    
    def add(self, key, value):
        """
        Add a value to the model's data.
        """
        self._data[key] = value

    def serialize(self) -> dict:
        """
        Returns a dictionnary of the data.
        """
        return self._data
