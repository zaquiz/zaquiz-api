from .DataModel import DataModel

class Entity(DataModel):
    """
    DataModel for Entity.
    """

    ID_NAME = "entityId"

    FIELDS = {
        "entityId": {
            "required": True,
            "types": (str,)
        },
        "type": {
            "required": True,
            "types": (str,)
        },
        "content": {
            "required": True,
            "types": (str,)
        },
        "sourceId": {
            "required": True,
            "types": (str,)
        }
    }

    @property
    def entityId(self) -> str:
        """
        The id of the entity.
        """
        return self._data.get("entityId")
    
    @property
    def type(self) -> str:
        """
        The type of entity (image, quote, etc).
        """
        return self._data.get("type")
    
    @property
    def content(self) -> str:
        """
        The content of the entity (the link to the image, the quote, etc).
        """
        return self._data.get("content")
    
    @property
    def sourceId(self) -> str:
        """
        The id of the source of the entity.
        """
        return self._data.get("sourceId")
    