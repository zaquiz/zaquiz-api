from flask import Blueprint, request, send_file, current_app
import werkzeug.exceptions as wkz

from ..dataservices import EntityService
from ..datamodels import Entity
from ..utils import FORBIDDEN_UPDATE

blueprint = Blueprint("entities", __name__, url_prefix="/api/v1/entities")

@blueprint.route('/', methods=['GET'], strict_slashes=False)
def get_all_entities():
    """
    Returns a list of all entities in the database.
    """
    
    return EntityService.find_all()

@blueprint.route('/', methods=['POST'], strict_slashes=False)
def post_entity():
    """
    Inserts one or several entities in the database.
    """
    
    # Raise 406 if no JSON is provided.
    if not request.is_json:
        raise wkz.NotAcceptable()

    json = request.get_json()
    
    if isinstance(json, list):
        # insert entities
        entities = [Entity(x) for x in json]
        EntityService.insert_many(entities)
    else:
        # insert one entity
        entity = Entity(request.get_json())
        EntityService.insert_one(entity)
    
    return '', 200

@blueprint.route('/random', methods=['POST'], strict_slashes=False)
def get_random_entity():
    """
    Returns a random entity. A JSON can be passed to filter the type of entity and its category. 
    """

    json = request.get_json() or {}
    entity : Entity = EntityService.find_random(categories=json.get("categories"), types=json.get("types"))

    # Raise 404 if nothing is found.
    if entity is None:
        raise wkz.NotFound()
    
    return entity

@blueprint.route('/id/<string:entity_id>', methods=['GET'], strict_slashes=False)
def get_one_entity(entity_id):
    """
    Returns the entity with the given id.
    """

    entity = EntityService.find_one(entity_id)
    asFile = str(request.args.get("asFile")).lower() == "true"

    # Raise 404 if nothing is found.
    if entity is None:
        raise wkz.NotFound()

    if asFile:
        filetype = entity.type
        filename = (current_app.config.get("RESOURCES_DIR") or "") + entity.content

        mimetype = ""
        if filetype == "image":
            ext = filename.split('.')[-1]
            if ext == "jpg":
                ext = "jpeg"
            mimetype = f"image/{ext}"
            
        if mimetype:
            return send_file(filename, mimetype=mimetype)

    return entity

@blueprint.route('/id/<string:entity_id>', methods=['PUT'], strict_slashes=False)
def edit_one_entity(entity_id):
    """
    Replaces the content of the entity with the given id.
    """

    # Raise 404 if nothing is found.
    if not EntityService.entity_exists(entity_id):
        raise wkz.NotFound()

    # Raise 406 if no JSON is provided.
    if not request.is_json:
        raise wkz.NotAcceptable()

    entity = Entity(request.get_json())

    # Raise 400 if id is updated.
    if entity.id() != entity_id:
        raise wkz.BadRequest(description=FORBIDDEN_UPDATE.format("entityId"))

    EntityService.update_one(entity)

    return '', 200

@blueprint.route('/id/<string:entity_id>', methods=['DELETE'], strict_slashes=False)
def delete_one_entity(entity_id):
    """
    Deletes the entity with the given id.
    """

    res = EntityService.find_one_and_delete(entity_id)

    # Raise 404 if nothing is found.
    if res is None:
        raise wkz.NotFound()
