from flask import Blueprint, request
import werkzeug.exceptions as wkz

from ..dataservices import CategoryService
from ..datamodels import Category
from ..utils import FORBIDDEN_UPDATE

blueprint = Blueprint("categories", __name__, url_prefix="/api/v1/categories")

@blueprint.route('/', methods=['GET'], strict_slashes=False)
def get_all_categories():
    """
    Returns a list of all categories in the database.
    """
    
    return CategoryService.find_all()

@blueprint.route('/', methods=['POST'], strict_slashes=False)
def post_category():
    """
    Inserts one or several categories in the database.
    """
    
    # Raise 406 if no JSON is provided.
    if not request.is_json:
        raise wkz.NotAcceptable()

    json = request.get_json()
    
    if isinstance(json, list):
        # insert categories
        categories = [Category(x) for x in json]
        CategoryService.insert_many(categories)
    else:
        # insert one category
        category = Category(request.get_json())
        CategoryService.insert_one(category)
    
    return '', 200

@blueprint.route('/id/<string:category_id>', methods=['GET'], strict_slashes=False)
def get_one_category(category_id):
    """
    Returns the category with the given id.
    """

    category = CategoryService.find_one(category_id)

    # Raise 404 if nothing is found.
    if category is None:
        raise wkz.NotFound()

    return category

@blueprint.route('/id/<string:category_id>', methods=['PUT'], strict_slashes=False)
def edit_one_category(category_id):
    """
    Replaces the content of the category with the given id.
    """

    # Raise 404 if nothing is found.
    if not CategoryService.entity_exists(category_id):
        raise wkz.NotFound()

    # Raise 406 if no JSON is provided.
    if not request.is_json:
        raise wkz.NotAcceptable()

    category = Category(request.get_json())

    # Raise 400 if id is updated.
    if category.id() != category_id:
        raise wkz.BadRequest(description=FORBIDDEN_UPDATE.format("categoryId"))

    CategoryService.update_one(category)

    return '', 200

@blueprint.route('/id/<string:category_id>', methods=['DELETE'], strict_slashes=False)
def delete_one_category(category_id):
    """
    Deletes the category with the given id.
    """

    res = CategoryService.find_one_and_delete(category_id)

    # Raise 404 if nothing is found.
    if res is None:
        raise wkz.NotFound()

@blueprint.route('/id/<string:category_id>/children', methods=['GET'], strict_slashes=False)
def get_category_children(category_id):
    """
    Returns a list of all categories whose parent has the given id.
    """

    # Raise 404 if it doesn't exist.
    if not CategoryService.entity_exists(category_id):
        raise wkz.NotFound()

    return CategoryService.find_all_children(category_id)
