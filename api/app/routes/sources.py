from flask import Blueprint, request
import werkzeug.exceptions as wkz

from ..dataservices import SourceService
from ..datamodels import Source
from ..utils import FORBIDDEN_UPDATE

blueprint = Blueprint("sources", __name__, url_prefix="/api/v1/sources")

@blueprint.route('/', methods=['GET'], strict_slashes=False)
def get_all_sources():
    """
    Returns a list of all sources in the database.
    """
    
    return SourceService.find_all()

@blueprint.route('/', methods=['POST'], strict_slashes=False)
def post_source():
    """
    Inserts one or several sources in the database.
    """
    
    # Raise 406 if no JSON is provided.
    if not request.is_json:
        raise wkz.NotAcceptable()

    json = request.get_json()
    
    if isinstance(json, list):
        # insert sources
        sources = [Source(x) for x in json]
        SourceService.insert_many(sources)
    else:
        # insert one source
        source = Source(request.get_json())
        SourceService.insert_one(source)
    
    return '', 200

@blueprint.route('/id/<string:source_id>', methods=['GET'], strict_slashes=False)
def get_one_source(source_id):
    """
    Returns the source with the given id.
    """

    source = SourceService.find_one(source_id)

    # Raise 404 if nothing is found.
    if source is None:
        raise wkz.NotFound()

    return source

@blueprint.route('/id/<string:source_id>', methods=['PUT'], strict_slashes=False)
def edit_one_source(source_id):
    """
    Replaces the content of the source with the given id.
    """

    # Raise 404 if nothing is found.
    if not SourceService.entity_exists(source_id):
        raise wkz.NotFound()

    # Raise 406 if no JSON is provided.
    if not request.is_json:
        raise wkz.NotAcceptable()

    source = Source(request.get_json())

    # Raise 400 if id is updated.
    if source.id() != source_id:
        raise wkz.BadRequest(description=FORBIDDEN_UPDATE.format("sourceId"))

    SourceService.update_one(source)

    return '', 200

@blueprint.route('/id/<string:source_id>', methods=['DELETE'], strict_slashes=False)
def delete_one_source(source_id):
    """
    Deletes the source with the given id.
    """

    res = SourceService.find_one_and_delete(source_id)

    # Raise 404 if nothing is found.
    if res is None:
        raise wkz.NotFound()
