from .categories import blueprint as bp_categories
from .sources import blueprint as bp_sources
from .entities import blueprint as bp_entities

BLUEPRINTS = [
    bp_categories,
    bp_sources,
    bp_entities
]