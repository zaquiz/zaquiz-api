from bson import ObjectId
from flask import jsonify, Response, Flask
from flask.json import JSONEncoder
from datetime import datetime

from .datamodels import DataModel

class CustomFlask(Flask):

    def make_response(self, rv):
        if isinstance(rv, dict) or isinstance(rv, list) or isinstance(rv, DataModel):
            rv = jsonify(rv)
        return super().make_response(rv)

class CustomEncoder(JSONEncoder):

    def default(self, obj):
        try:
            if isinstance(obj, DataModel):
                return obj.serialize()
            if isinstance(obj, ObjectId):
                return str(obj)
            if isinstance(obj, datetime):
                return obj.isoformat()
            iterable = iter(obj)
        except TypeError:
            pass
        else:
            return list(iterable)
        return JSONEncoder.default(self, obj)

class CustomResponse(Response):
    
    @classmethod
    def force_type(cls,rv,environ=None):
        if isinstance(rv, dict) or isinstance(rv, list) or isinstance(rv, DataModel):
            rv = jsonify(rv)
        return super(CustomResponse, cls).force_type(rv, environ)
    