from .custom import CustomEncoder, CustomResponse, CustomFlask
from .utils import ValidationFailed
from .dataservices import DataService

# The main Flask Application.
MAIN_APP = CustomFlask(__name__)

# Setup MAIN_APP settings

MAIN_APP.config.from_pyfile("../config.py")
MAIN_APP.json_encoder = CustomEncoder
MAIN_APP.response_class = CustomResponse
DataService.init_app(MAIN_APP)

# errorhandlers

@MAIN_APP.errorhandler(ValidationFailed)
def handle_validation_failed(err : ValidationFailed):
    """
    Error handler for ValidationFailed.
    """

    return err.message, 400

# Add blueprints

from .routes import BLUEPRINTS

for bp in BLUEPRINTS:
    MAIN_APP.register_blueprint(bp)
