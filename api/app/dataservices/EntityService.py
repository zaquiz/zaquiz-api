from random import randint

from .DataService import DataService
from .SourceService import SourceService
from .CategoryService import CategoryService, Category
from ..datamodels import Entity
from ..utils import NOT_EXISTING_REFERENCE

class EntityService(DataService):
    """
    A DataService for entities.
    """

    COLLECTION = "entities"
    DATA_MODEL = Entity
    
    @classmethod
    def validate(cls, document : Entity, other_docs=[]):
        """
        Returns a validation tuple (bool, str) indicating if the given document is valid.

        :param document: The document to check.

        :param other_docs: A list of documents about to be inserted before the current one. Default is an empty list.
        """
        validation = super().validate(document, other_docs)
        if not validation[0]:
            return validation
        
        sourceId = document.sourceId
        
        if not SourceService.entity_exists(sourceId):
            return (False, NOT_EXISTING_REFERENCE.format("sourceId", sourceId))
        
        return validation
    
    @staticmethod
    def __filter_entity(entity : Entity, categories : list):
        category : Category = CategoryService.find_one(
            SourceService.find_one(entity.sourceId).categoryId
        )
        
        return category.categoryId in categories
    
    @classmethod
    def find_random(cls, categories : list = None, types : list = None):
        """
        Returns a random entity which belongs to the given categories and types.

        :param categories: A list of the accepted categories. If None all categories are accepted.

        :param types: A list of the accepted types. If None all types are accepted.
        """
        query = {}
        if types is not None and isinstance(types, list):
            query["type"] = {"$in": types}
        
        entities = cls.find(query)
        if categories is not None and isinstance(categories, list):
            entities = [*filter(lambda x: EntityService.__filter_entity(x, categories), entities)]

        n = len(entities)
        entity : Entity = entities[randint(0, n-1)] if n > 0 else None
        if entity:
            source = SourceService.find_one(entity.sourceId)
            entity.add("names", source.names if source else [])
            entity.add("categoryId", source.categoryId)
        return entity
    
    @classmethod
    def find_one(cls, document_id : str) -> Entity:
        entity : Entity = super().find_one(document_id)
        source = SourceService.find_one(entity.sourceId)
        entity.add("names", source.names if source else [])
        entity.add("categoryId", source.categoryId)
        return entity
