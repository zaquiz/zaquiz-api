from flask_pymongo import PyMongo
from pymongo import ReturnDocument

from ..datamodels import DataModel
from ..utils import ValidationFailed, FIELD_MISSING_MSG, FIELD_WRONG_TYPE_MSG, DUPLICATE_ID_MSG

class DataService:
    """
    An abstract class to handle the access to the database.
    """

    __db = {}

    # The name of the collection. Must be overriden.
    COLLECTION = "collection"
    # The DataModel used for the service. Must be overriden.
    DATA_MODEL = DataModel
    
    @staticmethod
    def init_app(app):
        """
        Initializes the Flask application used to access the database.
        """
        DataService.__db = PyMongo(app).db
    
    @staticmethod
    def db():
        """
        Returns the current db.
        """
        return DataService.__db
    
    @staticmethod
    def drop_collection(collection,session=None):
        """
        Drops the collection with the given name.
        """
        DataService.db()[collection].drop(session)
    
    @classmethod
    def validate(cls, document : DataModel, other_docs=[]) -> tuple:
        """
        Returns a validation tuple (valid, msg) indicating if the given document is valid.

        :param document: The document to check.

        :param other_docs: A list of documents about to be inserted before the current one. Default is an empty list.
        """
        if not isinstance(document, cls.DATA_MODEL):
            return (False, f"Not a {cls.DATA_MODEL.__name__}")
        return document.validate()
    
    @classmethod
    def drop(cls, session=None):
        """
        Drops the collection linked to the service.
        """
        DataService.db()[cls.COLLECTION].drop(session)
    
    @classmethod
    def find(cls, query : dict) -> list:
        """
        Returns a list of all the entities matching the given query.

        :param query: A MongoDB query.
        """

        return [cls.DATA_MODEL(data) for data in DataService.db()[cls.COLLECTION].find(query)]
    
    @classmethod
    def find_all(cls) -> list:
        """
        Returns a list of all the entities contained in the collection.
        """
        return [cls.DATA_MODEL(data) for data in DataService.db()[cls.COLLECTION].find()]
    
    @classmethod
    def find_one(cls, document_id : str) -> DataModel:
        """
        Returns the document with the given id.
        """
        doc = DataService.db()[cls.COLLECTION].find_one({cls.DATA_MODEL.ID_NAME: document_id})
        return None if doc is None else cls.DATA_MODEL(doc)
    
    @classmethod
    def find_one_and_update(cls, document : DataModel, before=True):
        """
        Returns and update the given document.

        :param document: The document to update.

        :param before: Whether or not to return the corresponding document before its update.
        """
        validation = cls.validate(document)

        if not validation[0]:
            raise ValidationFailed(validation[1])
        
        return_doc = ReturnDocument.BEFORE if before else ReturnDocument.AFTER
        query = {cls.DATA_MODEL.ID_NAME : document.id()}
        update = {"$set" : document.serialize()}
        return DataService.db()[cls.COLLECTION].find_one_and_update(query, update, return_document=return_doc)
    
    @classmethod
    def find_one_and_delete(cls, document_id : str) -> DataModel:
        """
        Returns the document with the given id then deletes it from the collection.
        """
        doc = DataService.db()[cls.COLLECTION].find_one_and_delete({cls.DATA_MODEL.ID_NAME: document_id})
        return None if doc is None else cls.DATA_MODEL(doc)
    
    @classmethod
    def insert_one(cls, document : DataModel):
        """
        Inserts the given document in the collection.
        """
        validation = cls.validate(document)

        if not validation[0]:
            raise ValidationFailed(validation[1])
        
        if cls.entity_exists(document.id()):
            raise ValidationFailed(DUPLICATE_ID_MSG.format(cls.DATA_MODEL.ID_NAME, document.id()))
        
        DataService.db()[cls.COLLECTION].insert_one(document.serialize())
    
    @classmethod
    def insert_many(cls, documents : list):
        """
        Inserts all given documents in the collection.
        """
        ids = []
        docs = []
        doc : DataModel
        for doc in documents:
            validation = cls.validate(doc, docs)

            if not validation[0]:
                raise ValidationFailed(validation[1])
            
            doc_id = doc.id()
            if cls.entity_exists(doc_id) or doc_id in ids:
                raise ValidationFailed(DUPLICATE_ID_MSG.format(cls.DATA_MODEL.ID_NAME,doc_id))

            ids.append(doc_id)
            docs.append(doc)
        
        DataService.db()[cls.COLLECTION].insert_many([d.serialize() for d in documents])
    
    @classmethod
    def update_one(cls, document : DataModel, skip_validation=False):
        """
        Updates the content of the given document in the collection.
        """
        if not skip_validation:
            validation = cls.validate(document)

            if not validation[0]:
                raise ValidationFailed(validation[1])
        
        DataService.db()[cls.COLLECTION].update_one({cls.DATA_MODEL.ID_NAME: document.id()},{"$set": document.serialize()})
    
    @classmethod
    def update_many(cls, documents : list):
        """
        Updates the content of all given documents.
        """

        docs = []
        doc : DataModel
        for doc in documents:
            validation = cls.validate(doc, docs)

            if not validation[0]:
                raise ValidationFailed(validation[1])

            docs.append(doc)
        
        for doc in documents:
            cls.update_one(doc, skip_validation=True)
    
    @classmethod
    def count_documents(cls, query) -> int:
        """
        Returns the number of documents matching the given query.

        :param query: A mongoDB query.
        """
        return DataService.db()[cls.COLLECTION].count_documents(query)
    
    @classmethod
    def delete_many(cls, documents : list):
        """
        Deletes all given documents.
        """
        
        for doc in documents:
            cls.delete_one(doc)
    
    @classmethod
    def delete_one(cls, document : DataModel):
        """
        Deletes the given document from the collection.
        """
        return DataService.db()[cls.COLLECTION].delete_one({cls.DATA_MODEL.ID_NAME: document.id()})
    
    @classmethod
    def entity_exists(cls, id_value) -> bool:
        """
        Returns whether or not the document with the given id exists in the collection.
        """
        return cls.count_documents({cls.DATA_MODEL.ID_NAME: id_value}) > 0
