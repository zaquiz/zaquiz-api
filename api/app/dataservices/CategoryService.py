from .DataService import DataService
from ..datamodels import Category
from ..utils import NOT_EXISTING_REFERENCE

class CategoryService(DataService):
    """
    A DataService for categories.
    """

    COLLECTION = "categories"
    DATA_MODEL = Category
    
    @classmethod
    def validate(cls, document : Category, other_docs=[]):
        """
        Returns a validation tuple (bool, str) indicating if the given document is valid.

        :param document: The document to check.

        :param other_docs: A list of documents about to be inserted before the current one. Default is an empty list.
        """
        validation = super().validate(document, other_docs)
        if not validation[0]:
            return validation
        
        parent_id = document.parentId
        
        if parent_id is not None and not cls.entity_exists(parent_id) and parent_id not in [doc.categoryId for doc in other_docs]:
            return (False, NOT_EXISTING_REFERENCE.format("parentId", parent_id))
        
        return validation
    
    @classmethod
    def find_all_children(cls, parent_id : str):
        """
        Returns a list of all categories with the given parent_id.
        """

        return cls.find({"parentId": parent_id})
    
    @classmethod
    def check_parent(cls, document : Category, parent_id : str):
        """
        Returns whether if one the parents of the given category (or the category itselfs) matches the given parent_id.
        """
        
        if document.categoryId == parent_id or document.parentId == parent_id:
            return True
        
        if document.parentId is None:
            return False
        else:
            parent : Category = cls.find_one(document.parentId)
            return cls.check_parent(parent, parent_id)

