from .DataService import DataService
from .CategoryService import CategoryService
from ..datamodels import Source
from ..utils import NOT_EXISTING_REFERENCE

class SourceService(DataService):
    """
    A DataService for sources.
    """

    COLLECTION = "sources"
    DATA_MODEL = Source
    
    @classmethod
    def validate(cls, document : Source, other_docs=[]):
        """
        Returns a validation tuple (bool, str) indicating if the given document is valid.

        :param document: The document to check.

        :param other_docs: A list of documents about to be inserted before the current one. Default is an empty list.
        """
        validation = super().validate(document, other_docs)
        if not validation[0]:
            return validation
        
        categoryId = document.categoryId
        
        if not CategoryService.entity_exists(categoryId):
            return (False, NOT_EXISTING_REFERENCE.format("categoryId", categoryId))
        
        return validation
