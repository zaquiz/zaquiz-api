package com.example.zaquizapplication;

public interface AsyncResponse<T> {
    void onResponseFinished(T result);
}
