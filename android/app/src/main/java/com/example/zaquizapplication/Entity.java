package com.example.zaquizapplication;

import org.json.JSONArray;
import org.json.JSONObject;

public class Entity {
    private String entityId;
    private String type;
    private String content;
    private String sourceId;
    private String[] names;
    private String categoryId;

    public Entity(JSONObject obj) {
        try {
            this.entityId = obj.getString("entityId");
            this.type = obj.getString("type");
            this.content = obj.getString("content");
            this.sourceId = obj.getString("sourceId");
            JSONArray jsonNames = obj.getJSONArray("names");
            this.names = new String[jsonNames.length()];
            for (int i = 0; i < jsonNames.length(); i++) {
                this.names[i] = jsonNames.getString(i);
            }
            this.categoryId = obj.getString("categoryId");
        } catch (Exception e) {
            e.printStackTrace();
        } 
    }

    public String getEntityId() { return entityId; }

    public String getType() { return type; }

    public String getContent() { return content; }

    public String getSourceId() { return sourceId; }

    public String getCategoryId() { return categoryId; }

    public String[] getNames() { return names.clone(); }

}
