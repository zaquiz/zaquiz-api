package com.example.zaquizapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private String playerName;

    public static int entitiesToPlay = 5;
    public static String baseUrl = "http://e22f12fe.ngrok.io";
    public static int timeByImage = 30;
    public static String[] categories = null;
    public static boolean[] categoriesChecked = null;

    private CountDownTimer timer;
    private int time;
    private int score;
    private int nextScore;
    private int guessedRight = 0;
    private int entitiesPlayed = 0;
    private Entity currentEntity;

    private ImageView quizImageView;
    private TextView countTime;
    private TextView questionText;
    private TextView scoreText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent i = getIntent();
        String playerName = i.getStringExtra("playerName");
        if (playerName != null) {
            setTitle(getString(R.string.app_name) + "\t\t\t\t\t\t\t\tPlayer: " + playerName);
        }


        quizImageView = findViewById(R.id.imageView);
        countTime = findViewById(R.id.timer);
        questionText = findViewById(R.id.question);
        scoreText = findViewById(R.id.score);

        // Score
        score = 0;
        nextScore = 5;
        guessedRight = 0;

        scoreText.setText(String.format("%s %d", getString(R.string.score),score));

        EditText edit_txt = findViewById(R.id.answer_input);

        edit_txt.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (checkAnswer(v.getText().toString().trim().toLowerCase())){
                        score += nextScore;
                        scoreText.setText(String.format("%s %d", getString(R.string.score),score));
                        guessedRight++;
                        flashBackground(Color.GREEN);
                        timer.cancel();
                        questionText.setText("You guessed right !");
                        v.setText("");
                        (new Handler()).postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                fetchNextEntity();
                            }
                        }, 5000);
                        return false;
                    }
                    else {
                        flashBackground(Color.RED);
                        if (nextScore > 1){
                            nextScore--;
                        }
                        return true;
                    }
                }
                return false;
            }
        });
        entitiesPlayed = 0;
        fetchNextEntity();
    }

    public boolean checkAnswer(String answer){
        if (currentEntity != null){
            for (String name : currentEntity.getNames()){
                if (answer.equals(name.trim().toLowerCase())){
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void onBackPressed(){}

    public void flashBackground(int color){
        LinearLayout linearLayout = findViewById(R.id.linear_layout);
        ObjectAnimator anim = ObjectAnimator.ofInt(linearLayout, "backgroundColor", Color.WHITE, color,
                Color.WHITE);
        anim.setDuration(500);
        anim.setEvaluator(new ArgbEvaluator());
        anim.setRepeatMode(ValueAnimator.RESTART);
        anim.setRepeatCount(0);
        anim.start();
    }

    public void fetchNextEntity() {
        if (entitiesPlayed < entitiesToPlay) {
            entitiesPlayed++;
            currentEntity = null;
            countTime.setText("");
            questionText.setText("Waiting for next image...");
            quizImageView.setImageResource(R.drawable.question);
            nextScore = 5;
            new ZaquizHTTPTask(new AsyncResponse<Entity>() {
                @Override
                public void onResponseFinished(Entity result) {
                    if (result != null) {
                        currentEntity = result;
                        new ZaquizImageHTTPTask(new AsyncResponse<Bitmap>() {
                            @Override
                            public void onResponseFinished(Bitmap result) {
                                questionText.setText(String.format("Guess the %s", currentEntity.getCategoryId()));
                                quizImageView.setImageBitmap(result);
                                startATimer(timeByImage);
                            }
                        }).execute(String.format("%s/api/v1/entities/id/%s?asFile=true", baseUrl, currentEntity.getEntityId()));
                    } else {
                        stopActivity();
                    }
                }
            }).execute(String.format("%s/api/v1/entities/random", baseUrl));
        } else {
            stopActivity();
        }
    }

    public void stopActivity(){
        countTime.setText("Finished");
        ScoreActivity.guessedRight = guessedRight;
        ScoreActivity.score = score;
        ScoreActivity.guesses = entitiesPlayed;
        Intent intent = new Intent(MainActivity.this, ScoreActivity.class);
        startActivity(intent);
        finish();
    }

    public void startATimer(int timeInSec) {
        time = timeInSec;
        timer = new CountDownTimer(timeInSec * 1000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                countTime.setText(String.format("%s %ds", getString(R.string.remaining_time), time));
                time--;
            }
            @Override
            public void onFinish() {
                questionText.setText(String.format("The answer was %s.", currentEntity.getNames()[0]));
                countTime.setText("Finished");
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        fetchNextEntity();
                    }
                }, 5000);
            }
        }.start();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState, @NonNull PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

    }
}
