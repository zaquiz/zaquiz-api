package com.example.zaquizapplication;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ZaquizImageHTTPTask extends AsyncTask<String, Void, Bitmap> {

    private AsyncResponse<Bitmap> asyncResponse;

    public ZaquizImageHTTPTask(AsyncResponse<Bitmap> asyncResponse){
        this.asyncResponse = asyncResponse;
    }

    @Override
    protected Bitmap doInBackground(String... params){
        try {
            URL requestUrl = new URL(params[0]);
            HttpURLConnection connection = (HttpURLConnection) requestUrl.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap result){
        super.onPostExecute(result);
        asyncResponse.onResponseFinished(result);
    }
}
