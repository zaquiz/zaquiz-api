package com.example.zaquizapplication;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ZaquizCategoriesHTTPTask extends AsyncTask<String, Void, String[]> {

    private AsyncResponse<String[]> asyncResponse;

    public ZaquizCategoriesHTTPTask(AsyncResponse<String[]> asyncResponse){
        this.asyncResponse = asyncResponse;
    }

    @Override
    protected String[] doInBackground(String... uri) {
        String[] response = null;
        BufferedReader reader = null;
        try {
            URL url = new URL(uri[0]);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                return response;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
            }

            if (buffer.length() == 0) {
                return response;
            }

            JSONArray categories = new JSONArray(buffer.toString());
            response = new String[categories.length()];
            for (int i = 0; i < categories.length(); i++){
                response[i] = categories.getJSONObject(i).getString("categoryId");
            }
            urlConnection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    protected void onPostExecute(String[] result) {
        super.onPostExecute(result);
        asyncResponse.onResponseFinished(result);
    }
}
