package com.example.zaquizapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ScoreActivity extends AppCompatActivity {

    public static int guessedRight = 0;
    public static int guesses = 0;
    public static int score = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);
        TextView scoreText = findViewById(R.id.score);
        scoreText.setText(String.format("%s %d", getString(R.string.score), score));
        TextView guessText = findViewById(R.id.guessed_right_text);
        guessText.setText(String.format(getString(R.string.guessed_right), guessedRight, guesses));
    }

    public void goToTitle(View v){
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed(){}
}
