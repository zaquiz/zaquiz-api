package com.example.zaquizapplication;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;


public class HomeActivity extends AppCompatActivity {

    public static SharedPreferences sp;
    private String playerName = null;
    private EditText nameInput = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_layout);

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        MainActivity.baseUrl = String.format("http://%s.ngrok.io", sp.getString("ngrokId", "e22f12fe"));
        MainActivity.entitiesToPlay = sp.getInt("entitiesToPlay", 5);
        MainActivity.timeByImage = sp.getInt("timeByImage", 30);
        String categories = sp.getString("categories", null);
        if (categories != null) {
            MainActivity.categories = categories.split(":");
        }

        nameInput = (EditText) findViewById(R.id.player_name);
        String name = sp.getString("playerName", "");
        nameInput.setText(name);
        playerName = name;
        nameInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() == 0) {
                    playerName = null;
                } else if (s.length() > 0) {
                    playerName = s.toString();
                    SharedPreferences.Editor prefsEditor = HomeActivity.sp.edit();
                    prefsEditor.putString("playerName", playerName);
                    prefsEditor.commit();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void onClickStart(View v) {
        if (playerName != null) {
            Intent i = new Intent(this, MainActivity.class);
            i.putExtra("playerName", playerName);
            startActivity(i);
            finish();
        } else {
            TextView missingNameText = (TextView) findViewById(R.id.missing_name);
            missingNameText.setText(getString(R.string.missing_name));
        }

    }

    public void onClickSettings(View v) {
        Intent i = new Intent(this, SettingsActivity.class);
        startActivity(i);
    }

    @Override
    public void onBackPressed(){}


}
