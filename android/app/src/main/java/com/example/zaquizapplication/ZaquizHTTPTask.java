package com.example.zaquizapplication;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class ZaquizHTTPTask extends AsyncTask<String, Void, Entity> {

    private AsyncResponse<Entity> asyncResponse;

    public ZaquizHTTPTask(AsyncResponse<Entity> asyncResponse){
        this.asyncResponse = asyncResponse;
    }

    @Override
    protected Entity doInBackground(String... uri) {
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        Entity result = null;
        try {
            URL url = new URL(uri[0]);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");

            if (MainActivity.categories != null) {
                String[] categoriesSelected = MainActivity.categories.clone();
                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestProperty("Content-Type","application/json");

                // Make a json
                JSONObject parameters = new JSONObject();
                JSONArray categories = new JSONArray();
                for(int i = 0; i < categoriesSelected.length; i++) {
                    categories.put(categoriesSelected[i]);
                }
                parameters.put("categories", categories);
                String parametersStr = parameters.toString();
                byte[] outputInBytes = parametersStr.getBytes("UTF-8");

                // Put json in http body
                OutputStream os = urlConnection.getOutputStream();
                os.write(outputInBytes);
                os.close();
            }

            urlConnection.connect();

            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                return result;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0) {
                return result;
            }

            JSONObject jsonObject = new JSONObject(buffer.toString());
            result = new Entity(jsonObject);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void onPostExecute(Entity result) {
        super.onPostExecute(result);
        asyncResponse.onResponseFinished(result);
    }
}
