package com.example.zaquizapplication;


import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.InputType;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setttings_layout);

        TextView nbOfImagesTv = findViewById(R.id.nb_img_value);
        TextView imageDisplayTv = findViewById(R.id.delay_value);
        TextView ngrokValue = findViewById(R.id.ngrok_value);
        nbOfImagesTv.setText(String.valueOf(MainActivity.entitiesToPlay));
        imageDisplayTv.setText(String.valueOf(MainActivity.timeByImage)+"s");
        ngrokValue.setText(MainActivity.baseUrl.substring(7, 15));


    }

    public void onClickNbImages(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        input.setText(String.valueOf(MainActivity.entitiesToPlay));
        builder.setTitle(R.string.nb_image_title)
                .setMessage("Define the number of images for the party: (min: 1 - max: 100)")
                .setCancelable(false)
                .setView(input)
                .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String nbImage = input.getText().toString();
                        TextView tv = (TextView) findViewById(R.id.nb_img_value);
                        SharedPreferences.Editor prefsEditor = HomeActivity.sp.edit();
                        long parsedNb = Long.parseLong(nbImage);
                        if (parsedNb > 100) {
                            parsedNb = 100;
                        } else if (parsedNb <= 0) {
                            parsedNb = 1;
                        }
                        tv.setText(nbImage);
                        MainActivity.entitiesToPlay = (int)parsedNb;
                        prefsEditor.putInt("entitiesToPlay", (int)parsedNb);
                        prefsEditor.commit();
                    }
                })
                .setNegativeButton("Cancel", new
                        DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void onClickDelay(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        input.setText(String.valueOf(MainActivity.timeByImage));
        builder.setTitle(R.string.display_delay_title)
                .setMessage("Define the time of thinking: (min: 10s - max: 60s)")
                .setCancelable(false)
                .setView(input)
                .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String time = input.getText().toString();
                        TextView tv =  (TextView) findViewById(R.id.delay_value);
                        long parsedValue = Long.parseLong(time);
                        SharedPreferences.Editor prefsEditor = HomeActivity.sp.edit();
                        if (parsedValue > 60) {
                            parsedValue = 60;
                        } else if (parsedValue <= 10) {
                           parsedValue = 10;
                        }
                        tv.setText(time + "s");
                        MainActivity.timeByImage = (int)parsedValue;
                        prefsEditor.putInt("timeByImage", (int)parsedValue);
                        prefsEditor.commit();
                    }
                })
                .setNegativeButton("Cancel", new
                        DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void onClickCategories(View view) {
        new ZaquizCategoriesHTTPTask(new AsyncResponse<String[]>() {
            @Override
            public void onResponseFinished(final String[] result) {
                if (result == null) { return; }

                if (MainActivity.categories == null) {
                    MainActivity.categories = result.clone();
                }

                if (MainActivity.categoriesChecked == null){
                    boolean[] checkedItems = new boolean[result.length];
                    Arrays.fill(checkedItems, true);
                    MainActivity.categoriesChecked = checkedItems;
                }

                // Init categories checked
                final boolean[] categoriesChecked = initCheckedCategories(result, MainActivity.categories.clone());

                AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
                builder.setTitle(R.string.categories_selection)
                        .setMultiChoiceItems(result, categoriesChecked, new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i, boolean b) {
                                categoriesChecked[i] = b;
                            }
                        })
                        .setCancelable(false)
                        .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                List<String> confirmedCategories = new ArrayList<>();
                                for (int j = 0; j < categoriesChecked.length; j++) {
                                    if (categoriesChecked[j]) {
                                        confirmedCategories.add(result[j]);
                                    }
                                }

                                String[] confirmedCategoriesArray = confirmedCategories.toArray(new String[confirmedCategories.size()]);
                                SharedPreferences.Editor prefsEditor = HomeActivity.sp.edit();
                                if (confirmedCategories.size() == 0 || confirmedCategories.size() == result.length) {
                                    MainActivity.categories = null;
                                    prefsEditor.putString("categories", null);
                                } else {
                                    MainActivity.categories = confirmedCategoriesArray;
                                    prefsEditor.putString("categories", joinString(confirmedCategoriesArray, ":"));
                                }
                                MainActivity.categoriesChecked = categoriesChecked;
                                prefsEditor.commit();
                            }
                        })
                        .setNegativeButton("Cancel", new
                                DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        }).execute(String.format("%s/api/v1/categories", MainActivity.baseUrl));
    }

    public void onClickNgrok(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setText(MainActivity.baseUrl.substring(7, 15));
        builder.setTitle(R.string.ngrok_title)
                .setMessage("Copy here the value in your ngrok url. (e.g: 11f02f47 if your url is : http://11f02f47.ngrok.io)")
                .setCancelable(false)
                .setView(input)
                .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        SharedPreferences.Editor prefsEditor = HomeActivity.sp.edit();
                        String ngrokValue = input.getText().toString();
                        TextView tv = findViewById(R.id.ngrok_value);
                        tv.setText(ngrokValue);
                        MainActivity.baseUrl = String.format("http://%s.ngrok.io", ngrokValue);
                        prefsEditor.putString("ngrokId", ngrokValue);
                        prefsEditor.commit();
                    }
                })
                .setNegativeButton("Cancel", new
                        DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private String joinString(String[] array, String delimiter) {
        String res = "";
        for (int i = 0; i < array.length; i++) {
            res += array[i];
            if (i != array.length - 1) {
                res += delimiter;
            }
        }
        return res;
    }

    private boolean[] initCheckedCategories(String[] allCategories, String[] categoriesFiltered) {
        boolean[] res = new boolean[allCategories.length];
        if (allCategories.length == categoriesFiltered.length) {
            Arrays.fill(res, true);
        } else {
            for (int i = 0; i < allCategories.length; i++) {
                if(Arrays.asList(categoriesFiltered).contains(allCategories[i])) {
                    res[i] = true;
                } else {
                    res[i] = false;
                }
            }
        }
        return res;
    }
}
